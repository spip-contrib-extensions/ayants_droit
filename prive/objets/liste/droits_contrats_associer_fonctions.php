<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     Ayants droit
 * @copyright  2016
 * @author     Les Développements Durables
 * @licence    GNU/GPL v3
 * @package    SPIP\Ayantsdroit\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');