<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ayantsdroit_titre' => 'Ayants droit',

	// C
	'configurer_lier_objets_label' => 'Gérer les droits sur les contenus :',

	// T
	'titre_page_configurer_ayantsdroit' => 'Configurer les ayants droit',
);
