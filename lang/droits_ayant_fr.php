<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_droits_ayant' => 'Ajouter cet ayant droit',

	// C
	'champ_adresse_label' => 'Adresse',
	'champ_credits_explication' => 'Si une formulation plus précise est demandée, elle sera utilisé à la place du simple nom de l’ayant droit.',
	'champ_credits_label' => 'Crédits personnalisés',
	'champ_email_label' => 'Email',
	'champ_interlocuteur_label' => 'Interlocuteur/trice',
	'champ_nom_label' => 'Nom',
	'champ_telephone_label' => 'Téléphone',
	'confirmer_supprimer_droits_ayant' => 'Confirmez-vous la suppression de cet ayant droit ?',

	// I
	'icone_creer_droits_ayant' => 'Créer un ayant droit',
	'icone_modifier_droits_ayant' => 'Modifier cet ayant droit',
	'info_1_droits_ayant' => 'Un ayant droit',
	'info_aucun_droits_ayant' => 'Aucun ayant droit',
	'info_droits_ayants_auteur' => 'Les ayants droit de cet auteur',
	'info_nb_droits_ayants' => '@nb@ ayants droit',

	// R
	'retirer_lien_droits_ayant' => 'Retirer cet ayant droit',
	'retirer_tous_liens_droits_ayants' => 'Retirer tous les ayants droit',

	// S
	'supprimer_droits_ayant' => 'Supprimer cet ayant droit',

	// T
	'texte_ajouter_droits_ayant' => 'Ajouter un ayant droit',
	'texte_changer_statut_droits_ayant' => 'Cet ayant droit est :',
	'texte_creer_associer_droits_ayant' => 'Créer et associer un ayant droit',
	'texte_definir_comme_traduction_droits_ayant' => 'Cet ayant droit est une traduction de l’ayant droit numéro :',
	'titre_droits_ayant' => 'Ayant droit',
	'titre_droits_ayants' => 'Ayants droit',
	'titre_droits_ayants_rubrique' => 'Ayants droit de la rubrique',
	'titre_langue_droits_ayant' => 'Langue de cet ayant droit',
	'titre_logo_droits_ayant' => 'Logo de cet ayant droit',
);
